import scala.io.StdIn.readInt

object Fibo extends App {

    def Fib(n: Int):Int = n 
	match{
        case 0  => 0 
		case x if x==1 => 1
		case _ => Fib(n-1)+ Fib(n-2)
    }

	  def fiboSeq(n:Int) : Unit =
    { 
        if(n>0)
		fiboSeq(n-1)
		println(Fib(n))
    } 
	
	print("Enter a number: ")
    var p = scala.io.StdIn.readInt()
	
	fiboSeq(p)
}


