object Theater
{ 
	// Function define 
    def Attendees(r:Double ): Double=
    { 
        return 120+(15-r)/5*20;
    } 

	// Function define 
    def Revenue(r:Double ): Double=
    { 
       return Attendees(r)*r
    }
	
	// Function define 
    def Cost(r:Double ): Double=
    { 
       return 500 + 3 * Attendees(r) 
    }	
	
	// Function define 
    def Profit(r:Double ): Double=
    { 
       return Revenue(r)-Cost(r)
    }
	
	
      
    // Main method 
    def main(args:Array[String]) : Unit =
    { 
        println("If the ticket Price is 15,  Then the profit is: ",Profit(15)); 
		println("If the ticket Price is 10,  Then the profit is: ",Profit(10)); 
		println("If the ticket Price is 20,  Then the profit is: ",Profit(20)); 
    } 
} 