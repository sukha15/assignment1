class Account (id:String, n:Int, b:Double)
{
var nic:String = id
var acnumber:Int = n
var balance: Double = b


	def deposit(amount:Double) = {
	this.balance=this.balance+amount 
	
	}
	
	def withdraw(amount:Double) ={
	this.balance=this.balance-amount

	}
	
	
    def transfer(from:Account , to:Account, a:Double) ={
	
	from.withdraw(a)
	to.deposit(a)
	
	}


}

object	Account1 {
def main(args : Array[String]): Unit ={
val a1 = new Account("123",1997,10000) 
val a2 = new Account("xxx" , 1998 ,10)
a1.transfer(a1,a2,10000)
println(a2.nic+" : "+ a2.acnumber+ " : "+ a2.balance)
}
}