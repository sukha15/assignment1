import math.{sqrt,pow}

object myPoint {

def main(args: Array[String]):Unit=
{
val p1= new Point(2,3)
val p2= new Point(5,6)
val p3= p1.move(2,3)
val p4= p1.copy(a=9,b=8)
val p5= p1.distance(p2)


	println(p1)
	println(p2)
	println(p3)
	println(p1+p2)
	println(p5)
}
}


case class Point(a: Int, b:Int){
def x: Int =a
def y: Int =b


override def toString = "("+x+","+y+")"

	def move (dx: Int, dy:Int )= Point(this.x + dx, this.y + dy)
	def +(that:Point) = Point(this.x+that.x,this.y+that.y)

	def distance(other: Point): Double ={
	
	sqrt(pow(x-other.x,2) + pow(y-other.y,2))
	}
	
	
	}
	
	
	