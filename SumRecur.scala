import scala.io.StdIn.readInt

object SumRecur extends App {

    def Sum(n: Int):Int  =
	{
    if(n<=1)
	   return n;
	
	   return n + Sum(n-1);
    }
	
	print("Enter a number: ")
    var p = scala.io.StdIn.readInt()
	
    println(Sum(p));
    

}

