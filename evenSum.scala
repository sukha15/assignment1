import scala.io.StdIn.readInt

object evenSum extends App {

    def evenSum(n: Int):Int  = n
	match{ case x if(x<=1) =>0
	case x if(x%2==0) => n+evenSum(n-2)
	case _ => n-1 + evenSum(n-3)
    }
	
	print("Enter a number: ")
    var p = scala.io.StdIn.readInt()
	
    println(evenSum(p));
    

}

